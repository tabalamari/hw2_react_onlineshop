import React, { Component } from 'react';
import PropTypes from 'prop-types';


class ProductInCart extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="product-in-cart">
                <p className="title-in-cart">{this.props.productById.title}</p>
                <img className="image-in-cart" src={this.props.productById.src} />
                {this.props.productById.price}
            </div>
        )
    }
}

class CartDropdown extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const carts = [];
        for (let i = 0; i < localStorage.length; i++) {
            let localStorageKey = localStorage.key(i);
            if (localStorageKey.startsWith('fav_'))
                continue;
            const setnumber = localStorageKey.slice(5);
            const productById = this.props.getProductById(setnumber);
            if (productById) {
                carts.push(
                    <ProductInCart productById={productById} />
                )
            }
        }
        return (
        <div className={this.props.className}>{carts}</div>
        )
    }
}

class CartButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = { visible: false };
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        this.setState(state => ({
            visible: !state.visible
        }));
    }

    render() {
        return (
            <div>
                <button onClick={this.handleClick} className="cart"><i className="fa fa-shopping-basket" aria-hidden="true"></i>
                        Cart
                </button>
                <CartDropdown getProductById={this.props.getProductById} className={this.state.visible ? "dropdown" : "unvisible"} />
            </div>
        )
    }
}


class MenuTop extends React.Component {
    render() {
        return (
            <div className="menu-top__wrapper">
                <div className="menu-top">
                    <div className="icons">
                        <a href="#"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-telegram" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-twitter-square" aria-hidden="true"></i></a>
                    </div>
                    <div className="menu-top__right">
                        <button className="login">Login</button>/<button className="register">Register</button>
                        <CartButton getProductById={this.props.getProductById} />
                    </div>
                </div>
            </div>
        )
    }
}

MenuTop.propTypes = {
    productById: PropTypes.string,
    getProductById: PropTypes.func,
    className: PropTypes.string,
};

export default MenuTop;