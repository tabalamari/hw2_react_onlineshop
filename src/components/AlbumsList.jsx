import React, { Component } from 'react';
import Modal from './Modal';
import { FaStar } from 'react-icons/fa';
import PropTypes from 'prop-types';


class Images extends React.Component {
    render() {
        return (
            <img src={this.props.src}
                alt={this.props.alt}
            />)
    }
}

class Star extends React.Component {
    constructor(props) {
        super(props)
        this.state = { isFavorite: this.props.isFavorite };
        this.setFavorite = this.setFavorite.bind(this);
    }
    setFavorite() {
        this.setState(state => ({
            isFavorite: !state.isFavorite
        }));

        if (!this.state.isFavorite) {
            localStorage.setItem('fav_' + this.props.setnumber, 'yes');
        } else {
            localStorage.removeItem('fav_' + this.props.setnumber);
        }
    }

    render() {
        return (
            <FaStar onClick={this.setFavorite} color={this.state.isFavorite ? "orange" : "black"} size={30} />
        )
    }
}
class Product extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {

        return (
            <div>
                <div className="product-wrapper">
                    <Star
                        setnumber={this.props.setnumber}
                        isFavorite={localStorage.getItem('fav_' + this.props.setnumber) === 'yes'}
                    />
                    <Images src={this.props.src} />
                    <p>{this.props.title}</p>
                    <p>{this.props.text}</p>
                    <div className="price">
                        <p>{this.props.price}</p>
                        <AddButton header={this.props.title} price={this.props.price} text={this.props.text} src={this.props.src} setnumber={this.props.setnumber} />
                    </div>
                </div>
            </div>
        )
    }
}

class AddButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        };
        this.makeVisible = this.makeVisible.bind(this);
        this.makeInvisible = this.makeInvisible.bind(this);
    }

    makeVisible() {
        this.setState(state => ({
            visible: true
        }));
    }
    makeInvisible() {
        this.setState(state => ({
            visible: false
        }));
    }
    render() {
        return (
            <div>
                <button onClick={this.makeVisible} className="add">Add to cart</button>
                <Modal
                    makeInvisible={this.makeInvisible}
                    header={this.props.header}
                    price={this.props.price}
                    text={this.props.text}
                    src={this.props.src}
                    setnumber={this.props.setnumber}
                    className={this.state.visible ? "flex" : "unvisible"} />

            </div>
        )
    }
}

class AlbumsList extends React.Component {

    render() {
        let key = 0;
        const products = this.props.data.map(
            element => {
                const productItem = <Product
                    key={key++}
                    src={element.src}
                    title={element.title}
                    price={element.price}
                    text={element.text}
                    setnumber={element.setnumber} />;
                return productItem;
            });
        return (
            <div class="albums-list">
                <p className="albums-list__title">latest arrivals in musica</p>
                <div className="albums">
                    <div className="albums-list__product">
                        {products.map((item) => {
                            return item
                        })}
                    </div>
                </div>

                <div className="publishers">
                    <p className="albums-list__title">our most important publishers</p>

                    <div className="publishers-list">
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                        <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    </div>
                </div>
            </div >
        )
    }
}

AlbumsList.propTypes = {
    setnumber: PropTypes.number,
    alt: PropTypes.string,
    isFavorite: PropTypes.func,
    data: PropTypes.array,
    header: PropTypes.string,
    src: PropTypes.string,
    text: PropTypes.string,
    price: PropTypes.string,
    title: PropTypes.string,
};

export default AlbumsList;
