import React, { Component } from 'react';
import MenuTop from './MenuTop';
import Menu from './Menu';
import Carousel from './Carousel';
import PropTypes from 'prop-types';


class Header extends React.Component {
    render() {
        return (
            <header>
                <MenuTop getProductById = {this.props.getProductById} />
                <Menu />
                <Carousel />
            </header>)
    }
}

Header.propTypes = {
    getProductById: PropTypes.func,
};

export default Header; 