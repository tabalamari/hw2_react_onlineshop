import React from 'react';

class Menu extends React.Component {

    render() {
        return (
            <div className="menu-wrapper">
            <div className="menu">
                <img className="logo-img" src="./img/Vector Smart Object@1X.png" alt="Here is a logo" />
                <p className="logo-letter">M</p>
                <p class="logo-name">Store</p>
                <div className="menu-list">
                    <p>Home</p>
                    <p>CD'S</p>
                    <p>About us</p>
                    <p>Contacts</p>
                </div>
            </div>

        </div>
        )
    }
}

export default Menu;