import React, { Component } from 'react';

class FooterInfo extends React.Component {

    render() {
        return (
            <div className="footer-info__wrapper">
            <div className="footer-info">About us</div>
            <div className="footer-info">Our archives</div>
            <div className="footer-info">Popular posts</div>
            <div className="footer-info">About us</div>
        </div>
        )
    }
}

export default FooterInfo;