import React, { Component } from 'react';
import Title from './Title';
import AlbumsList from './AlbumsList';
import PropTypes from 'prop-types';


class Content extends React.Component {
    render() {
        return (
            <main className="content">
                <Title />
                <AlbumsList data={this.props.data}/>
            </main>)
    }
}
Content.propTypes = {
    data: PropTypes.array,
    };

export default Content;