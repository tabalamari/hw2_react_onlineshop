
import React from 'react';
import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';
import './App.css';


class App extends React.Component {
    constructor() {
        super();
        this.state = { data: [] };
        this.getProductById = this.getProductById.bind(this);
    };
    async componentDidMount() {
        const response = await fetch('./data.json');
        const json = await response.json();
        this.setState({ data: json });
    }
    getProductById(setnumber) {
        // alert(setnumber);
        const product = this.state.data.find(
            element => String(element.setnumber) === String(setnumber)
        );
        return product;
    }
    render() {

        return (
            <div className="App">
                <Header getProductById={this.getProductById} />
                <Content data={this.state.data} />
                <Footer />
            </div>
        );
    }
}

export default App;
