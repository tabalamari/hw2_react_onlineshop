import React, { Component } from 'react';

class Carousel extends React.Component {

    render() {
        return (
            <div className="carousel">
                <img className="carousel_img" src="img/headphones-with-red-heart-sign.jpeg" alt="Here is an image" />
        </div>
        )
    }
}

export default Carousel;