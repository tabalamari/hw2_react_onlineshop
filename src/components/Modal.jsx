import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }

    addToCart() {
        localStorage.setItem('cart_' + this.props.setnumber, 'yes');
    }

    render() {
        return (
            <div className={"modal_wrapper " + this.props.className}>
                <div className="modal">
                    <header className="modal_header" >
                        <span className="header_title">{this.props.header}</span>
                        <span className="icon_close" onClick={this.props.makeInvisible}>✕</span>
                    </header>
                    <div className="modal_content">
                        <p>{this.props.setnumber}</p>
                        <img src={this.props.src} />
                        <p>{this.props.text}</p>
                        <p>{this.props.price}</p>
                        <div className="modal_buttons">
                            <ModalButton onClick={this.props.makeInvisible} className="cancel_button" text="Cancel" />
                            <ModalButton onClick={this.addToCart} className="add_button" text="Add to cart" />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

class ModalButton extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <button onClick={this.props.onClick} className="modal-button">{this.props.text}</button>
            </div>
        )
    }
}
Modal.propTypes = {
    setnumber: PropTypes.number,
    className: PropTypes.string,
    header: PropTypes.string,
    makeInvisible: PropTypes.func,
    src: PropTypes.string,
    text: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func
};

export default Modal;


