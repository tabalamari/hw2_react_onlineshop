import React, { Component } from 'react';
import FooterInfo from './FooterInfo';
import SubFooter from './SubFooter';



class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <FooterInfo />
                <SubFooter />
            </footer>)
    }
}

export default Footer;